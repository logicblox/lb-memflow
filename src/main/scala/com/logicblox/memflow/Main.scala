package com.logicblox.memflow

import java.io.File
import java.net.URI

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

import com.logicblox.memflow.cli.Arguments
import com.logicblox.memflow.runtime._
import com.logicblox.workflow.lang._

object Main extends App {
  val arguments = Arguments.parse(args)

  val frontEndResult = FrontEnd.analyze(
    FrontEndInput(
      filepath = Some(URI.create("file://" + new File(arguments.filename).getAbsolutePath))
    )
  )

  val statusCode = frontEndResult match {
    case Right(system) => {
      val instantiator = new WorkflowInstantiator()

      system.findMainWorkflow() match {
        case Right(workflow) => {
          val mainProcess = instantiator.instance(workflow)

          val executor = WorkflowExecutor(arguments.bindings)

          val executionFuture = Await.ready(executor.execute(mainProcess, VariableObject.Root), Duration.Inf)
          val executionResult = executionFuture.value.get

          executor.cleanup()

          executionResult match {
            case Success(_) => 0
            case Failure(exception) => {
              exception.printStackTrace(System.err)
              1
            }
          }
        }
        case Left(exception) => {
          exception.printStackTrace(System.err)
          1
        }
      }
    }
    case Left(errors) => {
      CompilationError.printErrors(errors, System.err)
      1
    }
  }

  System.exit(statusCode)
}
