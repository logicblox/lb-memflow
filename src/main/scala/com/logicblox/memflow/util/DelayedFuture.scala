package com.logicblox.memflow.util

import java.util.{Timer, TimerTask}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

object DelayedFuture {

  private val timer = new Timer(true)

  def schedule[T](delayMilliseconds: Int)(body: => Future[T])(implicit context: ExecutionContext): Future[T] = {
    val p = Promise[T]()

    timer.schedule(new TimerTask {
      def run(): Unit = {
        context.execute(() => {
          Try(body) match {
            case Success(bodyFuture) => bodyFuture.onComplete(v => p.complete(v))
            case Failure(exception) => p.failure(exception)
          }
        })
      }
    }, delayMilliseconds)

    p.future
  }
}
