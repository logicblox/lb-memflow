package com.logicblox.memflow.cli

import java.io.File

import scala.collection.JavaConverters._
import com.beust.jcommander._
import com.logicblox.bloxweb.UsageException
import com.logicblox.memflow.runtime.PredicateBindings

class Arguments {
  @Parameter(
    names = Array("-f", "--file"),
    required = false,
    description = "The file containing the main workflow specification source.",
    validateValueWith = classOf[FileValidator])
  var filename: String = null

  @Parameter(
    names = Array("--binding"),
    required = false,
    description =
      "A string in the form 'predicate=value'. This will generate a string scalar predicate with this name, install it "
        + "in the workflow workspace and set its value.",
    validateWith = classOf[BindingsValidator]
  )
  var rawBindings: java.util.List[String] = new java.util.ArrayList[String]()

  lazy val bindings: PredicateBindings = {
    val bindingMap = rawBindings.asScala.map(binding => {
      val parts = binding.split("=")
      parts(0) -> parts(1)
    })
    .toMap

    PredicateBindings(bindingMap)
  }
}

object Arguments {

  def parse(argv: Array[String]): Arguments = {
    val arguments = new Arguments
    new JCommander(arguments, argv: _*)
    arguments
  }
}

class BindingsValidator extends IParameterValidator {

  def validate(name: String, value: String) = {
    if (value.split("=").length != 2)
      throw new UsageException(s"Invalid binding syntax for '$value': it must be predicate=value.")
  }
}

class FileValidator extends IValueValidator[String] {

  def validate(name: String, value: String) = {
    val file = new File(value)

    if (!file.exists) {
      throw new UsageException("File '" + file.getAbsolutePath + "' does not exist")
    }

    if (!file.isFile) {
      throw new UsageException("File '" + file.getAbsolutePath + "' is not a regular file")
    }

    if (!file.canRead) {
      throw new UsageException("Cannot read '" + file.getAbsolutePath + "'")
    }
  }
}
