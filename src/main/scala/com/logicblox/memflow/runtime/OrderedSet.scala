package com.logicblox.memflow.runtime

import com.logicblox.workflow.typesystem.ParameterValue

import scala.collection.JavaConverters._
import scala.collection.mutable

object OrderedSet {

  def apply(items: List[ParameterValue]): mutable.LinkedHashSet[ParameterValue] = {
    val sortedItems = items.sortBy(_.toString) // Mimics implicit lb-workflow behavior
    mutable.LinkedHashSet(sortedItems: _*)
  }

  def apply(item: ParameterValue): mutable.LinkedHashSet[ParameterValue] = {
    mutable.LinkedHashSet(item)
  }

  def apply(items: java.util.Set[_ <: ParameterValue]): mutable.LinkedHashSet[ParameterValue] = {
    apply(items.asScala.toList)
  }
}
