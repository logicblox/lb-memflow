package com.logicblox.memflow.runtime

import com.logicblox.workflow.lang.Instance
import com.logicblox.workflow.lang.Json._
import com.logicblox.workflow.typesystem.{JsonValue, StringValue}

import scala.collection.JavaConverters._

object JsonObjectEvaluator {

  def evaluate(input: Instance.JsonObject, vo: VariableObject): JsonValue = {
    JsonValue(evaluateMessage(input.value, vo))
  }

  private def evaluateMessage(message: JsonMessage, vo: VariableObject): JsonMessage = {
    message.hasElement() match {
      case true => {
        val evaluatedElement = evaluateElement(message.getElement, vo)

        JsonMessage
          .newBuilder(message)
          .setElement(evaluatedElement)
          .build
      }
      case false => message
    }
  }

  private def evaluateElement(element: JsonElement, vo: VariableObject): JsonElement = {
    if (element.hasReference) {
      val ref = element.getReference
      val evaluatedReference = evaluateReference(ref, vo)
      return JsonElement.newBuilder.setLiteral(evaluatedReference).build
    }

    if (element.hasObject) {
      val evaluatedObject = evaluateObject(element.getObject, vo)
      return JsonElement.newBuilder.setObject(evaluatedObject).build
    }

    if (element.hasArray) {
      val evaluatedArray = evaluateArray(element.getArray, vo)
      return JsonElement.newBuilder.setArray(evaluatedArray).build
    }

    val evaluatedLiteral = evaluateLiteral(element.getLiteral)
    JsonElement.newBuilder.setLiteral(evaluatedLiteral).build
  }

  private def evaluateLiteral(o: String) = o

  private def evaluateObject(o: JsonObject, vo: VariableObject): JsonObject = {
    val currentMembers = o.getMemberList.asScala

    val evaluatedMembers = currentMembers.map(currentMember => {
      val evaluatedMemberValue = evaluateElement(currentMember.getValue, vo)

      JsonMember
        .newBuilder
        .setKey(currentMember.getKey)
        .setValue(evaluatedMemberValue)
        .build
    })

    JsonObject
      .newBuilder
      .addAllMember(evaluatedMembers.asJava)
      .build
  }

  private def evaluateArray(o: JsonArray, vo: VariableObject): JsonArray = {
    val currentElements = o.getElementList.asScala
    val evaluatedElements = currentElements.map(element => evaluateElement(element, vo))

    JsonArray
      .newBuilder
      .addAllElement(evaluatedElements.asJava)
      .build
  }

  private def evaluateReference(ref: Reference, vo: VariableObject): String = {
    val variableScope = ref.getScope
    val variableName = ref.getVarname.substring(variableScope.length + 1)

    val variableDecl = Instance.VariableDecl(variableScope, variableName)
    val variableValue = vo.getVariableValue(variableDecl)

    variableValue.size match {
      case 1 =>
      case _ => throw new Exception("Set variables can't be used to construct JSON objects")
    }

    val variableActualValue = variableValue.head

    variableActualValue match {
      case v: StringValue => "\"" + variableActualValue + "\""
      case v: JsonValue => v.toString
      case other => throw new Exception(s"JSON objects can't reference values of unknown type: ${other.getClass}")
    }
  }
}


