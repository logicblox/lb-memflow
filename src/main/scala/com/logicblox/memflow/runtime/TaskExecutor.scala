package com.logicblox.memflow.runtime

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.JavaConverters._
import scala.util._
import com.logicblox.common.logging._
import com.logicblox.memflow.util.ListenableFutureUtils
import com.logicblox.tracing.internal.SilentSpan
import com.logicblox.workflow.lang.Instance
import com.logicblox.workflow.task.{AbstractTask, ResponseAction, TaskInput}
import com.logicblox.workflow.task.AsyncTask.AbstractAsyncTask
import com.logicblox.workflow.typesystem._

case class TaskExecutor(task: Instance.Task, vo: VariableObject, bindings: PredicateBindings, logger: Logger) {

  def execute(): Future[Unit] = {
    logger.info(s"Executing task '${task.info.sourceRef.description}'")

    instantiateTaskImpl() match {
      case Success(taskImpl) => {
        try {
          configureTaskImpl(taskImpl)

          executeAndCleanupTaskImplWithRetry(taskImpl).map(result => {
            processExecutionResult(result)
          })
        }
        catch {
          case e: Throwable => Future.failed(e)
        }
      }
      case Failure(e) => Future.failed(e)
    }
  }

  private def instantiateTaskImpl(): Try[AbstractTask] = {
    val implementationClassName = task.data
      .find(element => element._1 == TaskInput.TASK_IMPLEMENTATION_CLASS_PARAM)
      .map(element => element._2)
      .map(data => data.asInstanceOf[Instance.StringLiteral])
      .get

    try {
      val taskClass = Class
        .forName(implementationClassName.value)
        .newInstance()
        .asInstanceOf[AbstractTask]

      Success(taskClass)
    }
    catch {
      case e: Throwable => Failure(e)
    }
  }

  private def configureTaskImpl(taskImpl: AbstractTask): Unit = {
    val outputParameterNames = task.outputParams.map(p => p.id)
    val outputData = task.data.filter(data => outputParameterNames.contains(data._1))

    val requiredOutputs = task.outputParams.filter(p => !p.optional).map(p => p.id).toSet.asJava
    val optionalOutputs = task.outputParams.filter(p => p.optional).map(p => p.id).toSet.asJava

    val inputParameterNames = task.inputParams.map(p => p.id).toSet
    val inputData = task.data.filter(data => inputParameterNames.contains(data._1))

    val inputValues = inputData.map {
      case (paramName, paramValue) => {
        (paramName, DataEvaluator.evaluate(paramValue, vo, bindings).asJava)
      }
    }

    val outputValues = outputData.map {
      case (paramName, _) => {
        (s"${Constants.OPTIONAL_OUTPUT_BOUND_PREFIX}${paramName}", Set[ParameterValue]().asJava)
      }
    }

    val actualParameters = (inputValues ++ outputValues).asJava
      .asInstanceOf[java.util.Map[String, java.util.Set[_ <: ParameterValue]]]

    val span = SilentSpan.INSTANCE.child(s"Setting up task: ${task.description}".replace(" ", "_"))

    taskImpl.setup(
      "10000000000",
      "",
      task.info.sourceRef.description,
      null,
      actualParameters,
      requiredOutputs,
      optionalOutputs,
      span)
  }

  private def executeAndCleanupTaskImplWithRetry(taskImpl: AbstractTask): Future[ResponseAction] = {
    val driverMeta = DriverMetaParser.parse(taskImpl)

    val maxRetries = driverMeta match {
      case Some(meta) => meta.getRetry
      case None => 0
    }

    val retryInterval = driverMeta match {
      case Some(meta) => new Duration(meta.getRetryInterval)
      case None => new Duration("0s")
    }

    Scheduler.withRetry(maxRetries, retryInterval.msec)(
      taskImpl,
      taskImpl => executeAndCleanupTaskImpl(taskImpl),
      _ => logger.warn(s"Error executing task. Scheduling auto-retry. ${task.info.sourceRef.description}")
    )
  }

  private def executeTaskImpl(taskImpl: AbstractTask): Future[ResponseAction] = {
    taskImpl.start()

    val future = ListenableFutureUtils.asScala(taskImpl.execute())

    future.onComplete({
      case Failure(e) => logger.error(s"Error while executing task ${task.info.sourceRef.description}:\n${e}")
      case _ =>
    })

    future.transformWith({
      case Success(result) => {
        result.name match {
          case "success" => future
          case "fail" => Future.failed(new Exception("Task implementation returned failure status code"))
          case _ => Future.failed(new Exception("Task implementation returned unknown status code"))
        }
      }
      case Failure(_) => future
    })
  }

  private def cleanupTaskImpl(taskImpl: AbstractTask): Future[Unit] = {
    taskImpl match {
      case asyncTaskImpl: AbstractAsyncTask => {
        val cleanupFuture = ListenableFutureUtils.asScala(asyncTaskImpl.cleanup())
        cleanupFuture.flatMap(_ => Future.unit)
      }
      case _ => Future.unit
    }
  }

  private def executeAndCleanupTaskImpl(taskImpl: AbstractTask): Future[ResponseAction] = {
    val future = executeTaskImpl(taskImpl)

    future.transformWith({
      case Success(result) => cleanupTaskImpl(taskImpl).map(_ => result)
      case Failure(e) => cleanupTaskImpl(taskImpl).transformWith(_ => Future.failed(e))
    })
  }

  private def processExecutionResult(result: ResponseAction): Unit = {
    val outputParameterNames = task.outputParams.map(p => p.id)
    val outputData = task.data.filter(data => outputParameterNames.contains(data._1))

    outputData.foreach {
      case (argName, argDestination) =>
        val argValue = result.output.get(argName)
        val sortedArgValue = OrderedSet(argValue)

        argDestination match {
          case x: Instance.VariableRef => vo.setVariableValue(x.decl, sortedArgValue)
          case _ => throw new Exception(s"Task Executor encountered unknown output destination: ${argDestination.getClass}")
        }
    }
  }
}

object Constants {
  val OPTIONAL_OUTPUT_BOUND_PREFIX = "$"
}