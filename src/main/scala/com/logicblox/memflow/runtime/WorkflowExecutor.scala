package com.logicblox.memflow.runtime

import com.logicblox.common.logging._
import com.logicblox.workflow.lang.{Instance}
import com.logicblox.workflow.task.{AbstractTask}

import scala.concurrent.Future

case class WorkflowExecutor(bindings: PredicateBindings) {

  val logger: Logger = SystemDLogger.getLogger("Driver")

  def execute(process: Instance.Process, parentVo: VariableObject): Future[Unit] = {
    val vo = createVariableObject(process, parentVo)

    process match {
      case sequence: Instance.Seq => execute_Seq(sequence, vo)
      case parallel: Instance.Parallel => execute_Parallel(parallel, vo)
      case task: Instance.Task => execute_Task(task, vo)
      case forAll: Instance.ForAll => execute_ForAll(forAll, vo)

      case _ => throw new Exception(s"Workflow Executor encountered a process of unknown type: ${process.getClass}")
    }
  }

  def cleanup(): Unit = {
    AbstractTask.cleanupState()
  }

  private def execute_Seq(sequence: Instance.Seq, vo: VariableObject): Future[Unit] = {
    Scheduler.sequence(sequence.processes)(process => {
      execute(process, vo)
    })
  }

  private def execute_Parallel(parallel: Instance.Parallel, vo: VariableObject): Future[Unit] = {
    Scheduler.parallel(parallel.processes)(process => {
      execute(process, vo)
    })
  }

  private def execute_Task(task: Instance.Task, vo: VariableObject): Future[Unit] = {
    TaskExecutor(task, vo, bindings, logger).execute()
  }

  private def execute_ForAll(forAll: Instance.ForAll, vo: VariableObject): Future[Unit] = {
    LoopExecutor(forAll, vo, bindings).execute({
      case (body, bodyVo) => execute(body, bodyVo)
    })
  }


  private def createVariableObject(process: Instance.Process, parentVo: VariableObject): VariableObject = {
    val decls = process.vardecls

    if (decls.nonEmpty) {
      val vo = VariableObject(Some(parentVo))

      decls.foreach(decl => {
        vo.declareVariable(decl)
      })

      vo
    }
    else {
      parentVo
    }
  }
}
