package com.logicblox.memflow.runtime

import com.logicblox.memflow.util.DelayedFuture

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util._

object Scheduler {

  type Runner[T, V] = T => Future[V]
  type RetryCallback = Throwable => Unit


  def sequence[T](workItems: Iterable[T])(runner: Runner[T, Unit]): Future[Unit] = {
    var future = Future.unit

    for (workItem <- workItems) {
      future = future.flatMap(_ => runner(workItem))
    }

    future
  }

  def parallel[T](workItems: Iterable[T])(runner: Runner[T, Unit]): Future[Unit] = {
    val futures = workItems.map(workItem => runner(workItem))
    Future.sequence(futures).flatMap(_ => Future.unit)
  }

  def concurrent[T](limit: Int)(workItems: Iterable[T])(runner: Runner[T, Unit]): Future[Unit] = {
    limit match {
      case 1 => sequence(workItems)(runner)
      case Int.MaxValue => parallel(workItems)(runner)
      case _ => throw new Exception(s"Concurrency limit ${limit} is not supported")
    }
  }

  def withRetry[T, V](retryCount: Int, retryInterval: Int)(workItem: T, runner: Runner[T, V], onRetry: RetryCallback): Future[V] = {
    val attemptRetry = (exception: Throwable) => {
      if (retryCount == 0) {
        Future.failed(exception)
      } else {
        onRetry(exception)

        DelayedFuture.schedule(retryInterval)({
          withRetry(retryCount - 1, retryInterval)(workItem, runner, onRetry)
        })
      }
    }

    try {
      runner(workItem).transformWith({
        case Success(value) => Future.successful(value)
        case Failure(exception) => attemptRetry(exception)
      })
    }
    catch {
      case e: Throwable => attemptRetry(e)
    }
  }
}
