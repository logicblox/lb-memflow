package com.logicblox.memflow.runtime

import com.logicblox.workflow.lang.Instance
import com.logicblox.workflow.typesystem.{ParameterValue, StringValue}

import scala.collection.mutable

object DataEvaluator {

  def evaluate(value: Instance.Data, vo: VariableObject, bindings: PredicateBindings): mutable.LinkedHashSet[ParameterValue] = {
    value match {
      case v: Instance.StringLiteral => OrderedSet(StringValue(v.value))
      case v: Instance.StringTemplate => OrderedSet(StringTemplateEvaluator.evaluate(v, vo, bindings))
      case v: Instance.SetLiteral => OrderedSet(v.elements.flatMap(e => evaluate(e, vo, bindings)))
      case v: Instance.VariableRef => vo.getVariableValue(v.decl)
      case v: Instance.JsonObject => OrderedSet(JsonObjectEvaluator.evaluate(v, vo))
      case v: Instance.PredicateLiteral => OrderedSet(StringValue(bindings.get(v.name)))

      case _ => throw new Exception(s"Task Executor encountered an argument of unknown type: ${value.getClass}")
    }
  }

}
