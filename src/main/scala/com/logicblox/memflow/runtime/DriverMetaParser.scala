package com.logicblox.memflow.runtime

import java.io.ByteArrayInputStream

import com.googlecode.protobuf.format.JsonFormat
import com.logicblox.bloxweb.JsonFormatFactory
import com.logicblox.workflow.task.TaskConfig.DriverMeta
import com.logicblox.workflow.task.{AbstractTask, TaskInput}
import com.logicblox.workflow.typesystem.ParameterValue

object DriverMetaParser {

  def parse(taskImpl: AbstractTask) : Option[DriverMeta] = {
    val driverMetaRaw = taskImpl.getOptionalParameterValue(TaskInput.DRIVER_META_PARAM).orElse(null)

    Option(driverMetaRaw) match {
      case Some(x: ParameterValue) => {
        val builder = DriverMeta.newBuilder()
        val jsonFormatter = JsonFormatFactory.create(JsonFormat.Strictness.STRICT)
        val driverMetaBytes = new ByteArrayInputStream(x.toString().getBytes("UTF-8"))

        jsonFormatter.merge(driverMetaBytes, builder)

        Option(builder.build())
      }
      case None => None
    }
  }
}
