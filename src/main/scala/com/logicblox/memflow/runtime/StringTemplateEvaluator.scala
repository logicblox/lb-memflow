package com.logicblox.memflow.runtime

import com.logicblox.workflow.lang.Instance
import com.logicblox.workflow.typesystem.StringValue

object StringTemplateEvaluator {

  def evaluate(input: Instance.StringTemplate, vo: VariableObject, bindings: PredicateBindings): StringValue = {
    val specializedSegments = input.segments map(segment => evaluateSegment(segment, vo, bindings))
    val evaluatedString = specializedSegments.foldLeft("")((result, segment) => result + segment)

    StringValue(evaluatedString)
  }

  private def evaluateSegment(segment: Instance.Data, vo: VariableObject, bindings: PredicateBindings): String = {
    segment match {
      case Instance.StringLiteral(s) => s
      case Instance.PredicateLiteral(name, _) => bindings.get(name)
      case Instance.VariableRef(decl) => {
        val resolvedDecl = vo.resolveVariable(decl)
        val value = vo.getVariableValue(resolvedDecl)

        resolvedDecl.cardinality match {
          case Instance.Cardinality.SET => throw new Exception(s"Interpolation of set variables is not supported: ${decl.name}")
          case _ =>
        }

        value.head match {
          case StringValue(s) => s
          case other => throw new Exception(s"Variable values of unknown type: ${other.getClass}")
        }
      }
      case other => throw new Exception(s"Literal segment of unknown type: ${other.getClass}")
    }
  }
}
