package com.logicblox.memflow.runtime

case class PredicateBindings(bindings: Map[String, String]) {

  def get(name: String): String = {
    bindings.get(name) match {
      case Some(value) => value
      case None => throw new Exception(s"Predicate ${name} is not defined")
    }
  }
}
