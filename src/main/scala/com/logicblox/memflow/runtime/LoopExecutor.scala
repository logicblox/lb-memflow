package com.logicblox.memflow.runtime

import com.logicblox.workflow.lang.{AST, Instance}
import com.logicblox.workflow.typesystem.StringValue
import org.parboiled.support.Position

import scala.collection.mutable
import scala.concurrent.Future

case class LoopExecutor(forAll: Instance.ForAll, vo: VariableObject, bindings: PredicateBindings) {

  type LoopBodyExecutor = (Instance.Process, VariableObject) => Future[Unit]

  def execute(bodyExecutor: LoopBodyExecutor): Future[Unit] = {
    val paramDecl = forAll.param.decl

    val max = forAll.max match {
      case Some(m) => {
        val maxDecl = m.decl
        vo.declareVariable(maxDecl)

        val maxValue = DataEvaluator.evaluate(maxDecl.value.get, vo, bindings)
        vo.setVariableValue(maxDecl, maxValue)

        maxValue.head match {
          case StringValue(maxString) => maxString.toInt
          case other => throw new Exception(s"Unexpected type of max variable: ${other.getClass}")
        }
      }
      case None => Int.MaxValue
    }

    paramDecl.value match {
      case Some(paramValue) => {
        val iteratorValues = DataEvaluator.evaluate(paramValue, vo, bindings)

        Scheduler.concurrent(max)(iteratorValues)(iteratorValue => {
          val iteratorVarDecl = paramDecl.copy(
            cardinality = Instance.Cardinality.SCALAR,
            value = {
              val position = new Position(forAll.info.sourceRef.line, forAll.info.sourceRef.column)
              val astContext = AST.Context(position, position)

              Some(Instance.StringLiteral(iteratorValue.toString)(astContext))
            }
          )

          val bodyVo = VariableObject(Some(vo))
          bodyVo.declareVariable(iteratorVarDecl)
          bodyVo.setVariableValue(iteratorVarDecl, mutable.LinkedHashSet(iteratorValue))

          bodyExecutor(forAll.process, bodyVo)
        })
      }
      case None => throw new Exception(s"Iterator variable ${paramDecl.name} is not initialized")
    }
  }
}
