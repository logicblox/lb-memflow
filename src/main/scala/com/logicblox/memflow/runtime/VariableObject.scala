package com.logicblox.memflow.runtime

import com.logicblox.workflow.lang.Instance
import com.logicblox.workflow.typesystem.ParameterValue

import scala.annotation.tailrec
import scala.collection.mutable

case class VariableObject(parent: Option[VariableObject]) {

  private val declaredVariables = mutable.Map[String, Instance.VariableDecl]()

  private val variableValues = mutable.Map[String, mutable.LinkedHashSet[ParameterValue]]()


  def declareVariable(variable: Instance.VariableDecl): Unit = {
    declaredVariables(variable.qualifiedName) = variable
  }

  def resolveVariable(variable: Instance.VariableDecl): Instance.VariableDecl = {
    lookupVariableObject(variable) match {
      case Some(vo) => vo.declaredVariables(variable.qualifiedName)
      case None => throw new Exception(s"Variable ${variable.name} is undefined")
    }
  }

  def getVariableValue(variable: Instance.VariableDecl): mutable.LinkedHashSet[ParameterValue] = {
    lookupVariableObject(variable) match {
      case Some(vo) => vo.getOwnVariableValue(variable)
      case None => throw new Exception(s"Variable ${variable.name} is undefined")
    }
  }

  def setVariableValue(variable: Instance.VariableDecl, value: mutable.LinkedHashSet[ParameterValue]): Unit = {
    lookupVariableObject(variable) match {
      case Some(vo) => vo.setOwnVariableValue(variable, value)
      case None => throw new Exception(s"Variable ${variable.name} is undefined")
    }
  }


  private def getOwnVariableValue(variable: Instance.VariableDecl): mutable.LinkedHashSet[ParameterValue] = {
    variableValues.get(variable.qualifiedName) match {
      case Some(value) => value
      case None => throw new Exception(s"Variable ${variable.name} is undefined")
    }
  }

  private def setOwnVariableValue(variable: Instance.VariableDecl, value: mutable.LinkedHashSet[ParameterValue]): Unit = {
    variableValues(variable.qualifiedName) = value
  }


  @tailrec
  private def lookupVariableObject(variable: Instance.VariableDecl): Option[VariableObject] = {
    declaredVariables.get(variable.qualifiedName) match {
      case Some(_) => Some(this)
      case None => {
        this.parent match {
          case Some(vo) => vo.lookupVariableObject(variable)
          case None => None
        }
      }
    }
  }
}

object VariableObject {
  val Root = VariableObject(None)
}
