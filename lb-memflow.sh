#!/usr/bin/env bash

if [[ -z "$LB_COMPONENTS" ]]; then
	>&2 echo "LB_COMPONENTS environment variable is not defined (have you executed env.sh?)"
	exit 1
fi

DEPENDENCIES=$(find lib -name *.jar | awk -vORS=: '{ print $1 }' | sed 's/:$/\n/')

/opt/logicblox/lb-universe-deps/scala/bin/scala -classpath "target/scala-2.12/lb-memflow_2.12-0.1.jar:${DEPENDENCIES}" com.logicblox.memflow.Main $@