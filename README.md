# lb-memflow

Memflow is an in-memory interpreter of LB Workflow scripts. It operates entirely in-memory and prefers performance over resilience.

## Why?

The project exists to address the needs of OLTP systems, where overhead of a workflow is comparable to runtime of useful work. The most typical example are periodic workflows, that are frequently scheduled, short-lived and do not require resiliency of long-running batches. Developers will also benefit from greatly reduced execution time when deploying a project locally (Price Management project observed reduction in deployment time from 16 to 5 minutes).

This is how memflow addresses challenges of lb-workflow:

| lb-workflow challenge | lb-memflow solution |
|---------| -------- |
| Polling-based architecture implies periods of inactivity | No polling involved, the intereter knows exactly when previous process has finished executing |
| Complex Driver / Database communication protocol that requires 3 transactions per each poll | Memflow executes in memory
| Workflow progress is recorded in a database in recursive fashion, which is problematic for deeply nested or highly iterative workflows | Memflow doesn't record workflow progress. It executes by live traversing through AST-like structure |

## Usage

Command line arguments mimic the behavior of `lb workflow run`:

```./lb-memflow.sh -f test/workflows/forall.wf --binding a=Test```

![video.gif](https://bitbucket.org/repo/ooqy9K6/images/1563163873-video.gif)

## Benchmark

In this section we show the comparison of runtime of lb-workflow versus lb-memflow.

| Workflow | Project | lb-workflow | lb-memflow |
|----------|---------|-------------|------------|
| [Deploy](https://bitbucket.org/logicblox/price-mgmt/src/default/src/workflow/ops/deploy/deploy-initial.wf)| Price Management | 20m 21s | 9m 1s |
| [Deploy (PE disabled)](https://bitbucket.org/logicblox/price-mgmt/src/default/src/workflow/ops/deploy/deploy-initial.wf) | Price Management | 16m 19s | 5m 52s |
| [Import Cost Changes](https://bitbucket.org/logicblox/price-mgmt/src/default/src/workflow/main/import-cost-changes.wf) | Price Management | 16s | 2.7s |
| [Export Price Changes](https://bitbucket.org/logicblox/price-mgmt/src/default/src/workflow/main/export-price-changes.wf) | Price Management | 38s | 8s |