#!/usr/bin/env bash

# Upstream Directory
export LB_COMPONENTS=$PWD/upstream

export LOGICBLOX_HOME=$LB_COMPONENTS/logicblox

# Bloxweb Home
export BLOXWEB_HOME=$LB_COMPONENTS/lb-web
export LB_WEBSERVER_HOME=${BLOXWEB_HOME}
# lb-workflow
export LB_WORKFLOW_HOME=$PWD/out
# Path
export LB_LIBRARY_PATH=$LB_WEBSERVER_HOME/share:$LB_LIBRARY_PATH
export PATH=${PWD}:${PWD}/bin:${LOGICBLOX_HOME}/bin:${BLOXWEB_HOME}/bin:${LB_WORKFLOW_HOME}/bin:${PATH}
# Python Path
export PYTHONPATH=${LOGICBLOX_HOME}/lib/python:${BLOXWEB_HOME}/lib/python:${PYTHONPATH}

# needed to run tasks that use lb-web-client jars
export LB_WEBCLIENT_HOME=$LB_WEBSERVER_HOME

# needed for lb-workflow run command
export LB_CONNECTBLOX_ENABLE_ADMIN=1

if [[ -f $LOGICBLOX_HOME/libexec/python-argcomplete.sh ]]; then
  source $LOGICBLOX_HOME/libexec/python-argcomplete.sh
fi

#my_dir=`dirname $0`
  #above doesn't work with vagrant/virtualbox
my_dir=`pwd`

if [[ -f $my_dir/local/env.sh ]];
then
  source $my_dir/local/env.sh
fi
